# Тестовое задание


## Installation
1. Install Composer using detailed installation instructions [here](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
2. Install Node.js using detailed installation instructions [here](https://nodejs.org/en/download/package-manager/)
3. Clone repository
```
$ git clone https://gitlab.com/AzAzzAzzz/test-blog.git
```
4. Change into the working directory
```
$ cd test-blog
```
5. Copy `.env.example` to `.env` and modify according to your environment
```
$ cp .env.example .env
```
6. Install composer dependencies
```
$ composer install --prefer-dist
```
7. Execute following commands to install other dependencies
```
$ npm install
$ npm run dev
```
8. Run these commands to create the tables within the defined database and populate seed data
```
$ php artisan migrate --seed
$ php artisan db:seed --class=ArticlesTableSeeder
```
Sample users:
```
Admin user: admin.laravel@labs64.com / password: admin
```