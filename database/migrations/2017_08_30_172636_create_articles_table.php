<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table
                ->string('name', 255)
                ->comment('Наименование поста');
            $table
                ->string('alias', 255)
                ->comment('Системное имя поста');
            $table
                ->text('description')
                ->comment('Краткое описание (превьюшка)');
            $table
                ->text('content')
                ->comment('Контент поста');
            $table
                ->tinyInteger('active')
                ->default(0)
                ->comment('Статус активности поста 1 - пост активен,  0 - не активен');
            $table->unique('alias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
