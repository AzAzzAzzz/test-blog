<?php

use Illuminate\Database\Seeder;
use App\Models\Blog\Article;
use Spatie\MediaLibrary\Media;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i <= 50; $i++) {
            $article = new Article();

            $article->name        = $faker->realText(rand(30, 60));
            $article->alias       = $faker->unique()->slug(3);
            $article->content     = '<p>' . $faker->realText(rand(350, 500)) . '</p>';
            $article->content    .= '<p>' . $faker->realText(rand(350, 500)) . '</p>';
            $article->content    .= '<p>' . $faker->realText(rand(350, 500)) . '</p>';
            $article->content    .= '<p>' . $faker->realText(rand(350, 500)) . '</p>';
            $article->content    .= '<p>' . $faker->realText(rand(350, 500)) . '</p>';
            $article->description = $faker->realText(rand(150, 250));
            $article->active      = 1 ;

            // Добавление данных статьи
            $article->save();

            // Привязка файла
            $article->addMediaFromUrl($faker->imageUrl(700, 300))
                ->withCustomProperties(['article_id' => $article->id])
                ->toMediaLibrary('images');

            print "Added entry - $i \n";
        }
    }
}
