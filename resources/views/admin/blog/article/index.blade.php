@extends('admin.layouts.admin')

@section('title', 'Посты')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Список статей</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li>
                            <a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="articles-table"
                           class="table table-striped table-bordered dt-responsive nowrap"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Наименование</th>
                            <th>Системное имя</th>
                            <th>Статус</th>
                            <th>Добавлено</th>
                            <th>Обнавлено</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Наименование</th>
                            <th>Системное имя</th>
                            <th>Статус</th>
                            <th>Добавлено</th>
                            <th>Обнавлено</th>
                            <th>Действия</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Шаблон предупреждения об удалении страницы --}}
    <script id="destroy-article" type="text/template">
        <p>Удалить статью "@{{ name }}"?</p>
    </script>

    {{-- Шаблон статуса активности в колонках таблицы --}}
    <script id="rowActiveColumn" type="text/template">
        @{{#active}}
        <span class="label label-primary">Активный</span>
        @{{/active}}
        @{{^active}}
        <span class="label label-danger">Не активный</span>
        @{{/active}}
    </script>

    {{-- Шаблон кнопок в колонках таблицы --}}
    <script id="actionButtonsTemplate" type="text/template">
        <a class="btn btn-xs btn-info"
           target="_blank"
           href="/admin/article/@{{ id }}/edit"
           data-toggle="tooltip"
           data-placement="top"
           title="Посмотреть  |  Редактировать">
            <i class="fa fa-pencil"></i>
        </a>

        @{{#active}}
        <button class="btn btn-xs btn-success btn-post-active"
                data-toggle="tooltip"
                data-placement="top"
                title="Активен"
                data-active="@{{active}}"
                data-id="@{{id}}">
            <i class="fa fa-unlock" aria-hidden="true"></i>
        </button>
        @{{/active}}
        @{{^active}}
        <button class="btn btn-xs btn-danger btn-post-active"
                data-toggle="tooltip"
                data-placement="top"
                title="Не активен"
                data-active="@{{active}}"
                data-id="@{{id}}">
            <i class="fa fa-lock" aria-hidden="true"></i>
        </button>
        @{{/active}}

        <button class="btn btn-xs btn-danger btn-post-destroy"
                data-toggle="tooltip"
                data-placement="top"
                title="Удалить"
                data-id="@{{ id }}"
                data-article-name="@{{ name }}">
            <i class="fa fa-trash"></i>
        </button>
    </script>

@endsection


@section('scripts')
    @parent

    {{ Html::script(mix('assets/admin/js/blog/article/index.js')) }}
@endsection