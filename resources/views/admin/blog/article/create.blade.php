@extends('admin.layouts.admin')

@section('title', 'Новый статья')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Добавление новой статьи</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">

                        <div class="col-sm-12">
                            <div id="alert" class="alert alert-danger fade in col-sm-8 col-sm-offset-3 hide">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <div class="messages"></div>
                            </div>
                        </div>

                        <form id="create-article-form" action="{{ route('admin.article.store') }}" class="form-horizontal form-label-left " enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Наименование
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text"
                                           name="name"
                                           id="name"
                                           class="form-control"
                                           value=""
                                           placeholder="Введите наименование..">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Системное имя
                                    <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <input type="text"
                                           name="alias"
                                           id="alias"
                                           class="form-control"
                                           value=""
                                           placeholder="Введите системное имя..">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Изображение
                                    <span class="required">*</span>
                                </label>
                                <div class="col-sm-8 ">
                                    {{ csrf_field() }}
                                    <div class="">
                                        <div id="file-area" class="file-area">
                                            <input id="file-upload"
                                                   class="box__file"
                                                   type="file"
                                                   name="media"
                                                   accept="image/gif, image/jpeg, image/png, image/jpg">
                                            <div class="file-dummy">
                                                <span class="success">
                                                Выберите или перетащите файлы сюда
                                                </span>
                                                <span class="default">
                                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    Выберите или перетащите файлы сюда
                                                </span>
                                            </div>
                                        </div>
                                        <label id="file-upload-error" class="error" for="file-upload"></label>
                                        <div id="files" class="files"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Описание
                                    <span class="required">*</span>
                                </label>
                                <div class="col-sm-8">
                                    <textarea name="description"
                                              id="description"
                                              class="form-control"
                                              rows="8"></textarea>
                                    <label id="description-error" class="error" for="description"></label>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Контент
                                    <span class="required">*</span>
                                </label>
                                <div class="col-sm-8">

                                    <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#content">
                                        <div class="btn-group">
                                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                            </ul>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a data-edit="fontSize 5">
                                                        <p style="font-size:17px">Huge</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-edit="fontSize 3">
                                                        <p style="font-size:14px">Normal</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-edit="fontSize 1">
                                                        <p style="font-size:11px">Small</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                            <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn btn-info" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                            <div class="dropdown-menu input-append">
                                                <input class="span2" placeholder="URL" type="text" data-edit="createLink">
                                                <button class="btn" type="button">Add</button>
                                            </div>
                                            <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                        </div>
                                    </div>
                                    <div id="content" class="editor-wrapper placeholderText" contenteditable="true"></div>
                                    <textarea name="content" id="content" class="hide"></textarea>

                                    <label id="content-error" class="error" for="content"></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Активен</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="switch-active"
                                           class="switch"
                                           type="checkbox"
                                           name="active"
                                           value="0"
                                           data-on-text="Да"
                                           data-off-text="Нет">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-primary" type="reset">Очистить</button>
                                    <button type="submit" id="button" class="btn btn-success">Сохранить</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script type="text/template" id="itemTemplate">
        <div class="preview item row">
            <div class="thumbnail">
                <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="@{{ src }}">
                    <div class="mask">
                        <p>@{{ name }}</p>
                        <div class="tools tools-bottom">
                            <a href="" class="btn-img-delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="caption">
                    <p><strong>@{{ name }}</strong></p>
                    <p></p>
                </div>
            </div>
        </div>
    </script>
    {{ Html::script(mix('assets/admin/js/blog/article/create.js')) }}
@endsection
