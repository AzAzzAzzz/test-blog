
@foreach($articles as $article)
    <div class="row">
        <div class="col-md-7">
            <a href="{{ route('article.post',  $article->alias) }}" target="_blank">
                <img class="img-fluid rounded mb-3 mb-md-0 responsive" src="{{ $article->media[0]->getUrl() }}" alt="">
            </a>
        </div>
        <div class="col-md-5">
            <h3>{{ $article->name }}</h3>
            <p>{{ $article->description }}</p>
            <a class="btn btn-primary" href="{{ route('article.post',  $article->alias) }}" target="_blank">Далее</a>
        </div>
    </div>

    <hr>
@endforeach

<div class="row text-center">
    {{ $articles->links() }}
</div>