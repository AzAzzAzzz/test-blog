@extends('layouts.app')

@section('title', $article->name)

@section('page')
    <div class="container">
        <h1 class="my-4">
            {{ $article->name }}
        </h1>

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <img class="img-fluid rounded mb-3 mb-md-0 responsive" src="{{ $article->media[0]->getUrl() }}" alt="">
                </div>
                <div class="col-md-12">
                    <br>
                    <p class="datetime">{{ $article->created_at }}</p>
                    <p>{!! $article->content !!}</p>
                </div>
            </div>
        </div>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/app/css/blog/blog.css')) }}
@endsection