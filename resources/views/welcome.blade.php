@extends('layouts.app')

@section('title', 'Главная')

@section('page')

	<div class="container">
		<div class="row">
		    <div class="col-sm-12 text-center">
				@if (Route::has('login'))
					@if (!Auth::check())
						@if(config('auth.users.registration'))
							<a href="{{ url('/register') }}">{{ __('views.welcome.register') }}</a> |
						@endif
							<a href="{{ url('/login') }}">{{ __('views.welcome.login') }}</a>
						@else
							@if(auth()->user()->hasRole('administrator'))
								<a href="{{ url('/admin') }}">{{ __('views.welcome.admin') }}</a>
							@endif
								<a href="{{ url('/logout') }}">{{ __('views.welcome.logout') }}</a>
						@endif
				@endif
			</div>
		</div>	
    </div>	


    @if( ! empty($articles))
        <div class="container">
            <h1 class="my-4">
                Список статей
            </h1>

            <div class="content">
                @foreach($articles as $article)
                    <div class="row">
                        <div class="col-md-7">
                            <a href="{{ route('article.post',  $article->alias) }}" target="_blank">
                                <img class="img-fluid rounded mb-3 mb-md-0 responsive" src="{{ $article->media[0]->getUrl() }}" alt="">
                            </a>
                        </div>
                        <div class="col-md-5">
                            <h3>{{ $article->name  }}</h3>
                            <p class="datetime">{{ $article->created_at }}</p>
                            <p>
                                {{ $article->description }}
                            </p>
                            <a class="btn btn-primary" href="{{ route('article.post',  $article->alias) }}" target="_blank">Далее</a>
                        </div>
                    </div>

                    <hr>
                @endforeach

                <div class="row text-center">
                    {{ $articles->links() }}
                </div>
            </div>
    @else
        <p class="alert-danger text-center">Не удалось загрузить данные</p>
    @endif
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/app/css/blog/blog.css')) }}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/app/js/blog/index.js')) }}
@endsection