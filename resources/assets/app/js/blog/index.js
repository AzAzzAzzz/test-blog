jQuery(function () {
    jQuery('body').on('click', '.pagination a', function (e) {
        e.preventDefault();
        var url = '/ajax/article?page=' + jQuery(this).attr('href').split('page=')[1];

        jQuery.get(url, function (html) {
            jQuery('.content').empty().html(html);
        })
    });
});