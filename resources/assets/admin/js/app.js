_token = jQuery('meta[name="csrf-token"]').attr('content');



var inputUpload = jQuery('#file-upload'),
    uploadedFiles = jQuery('#files');

/**
 *  Превью изображения
 */
inputUpload.change(function (e) {
    jQuery.each(e.target.files, function(index, file) {
        var reader = new FileReader(),
            template = jQuery('#itemTemplate').html();

        uploadedFiles.empty();
        Mustache.parse(template);

        reader.onload = function(e) {
            object = {};
            object.filename = file.name;
            object.data = e.target.result;
            uploadedFiles.append(Mustache.render(template, {src: object.data, name: file.name}));
        };
        reader.readAsDataURL(file);
    });
});

/**
 * Вывод сообщений об ошибке
 * @param content
 * @param title
 */
function errorMessage (content, title) {
    title = title || 'Предупреждение';
    content = content || 'Упс.. Что-то пошло не так';

    jQuery.confirm({
        title: title,
        content: content,
        type: 'red',
        typeAnimated: true,
        buttons: {
            'Закрыть' : function () {
            }
        }
    });
}

/**
 *  Вывод сообщений об успешном завершении действия
 * @param content
 * @param title
 */
function sucessMessage (content, title) {
    title = title || 'Оповещение';
    content = content || 'Запрашиваемое действие успешно выполнено';

    jQuery.confirm({
        title: title,
        content: content,
        type: 'dark',
        typeAnimated: true,
        buttons: {
            'Закрыть' : function () {
            }
        }
    });
}

/**
 * Инициализация Переключачтелей
 */
jQuery('.switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function(event, state) {
    if (state) {
        jQuery(this).val(1);
    } else {
        jQuery(this).val(0);
    }
});