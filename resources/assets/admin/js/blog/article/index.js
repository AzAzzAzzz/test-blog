jQuery(function () {
    var table =  jQuery('#articles-table'),
        dateFormat = 'DD MMMM YYYY в hh:mm';

    table.DataTable({
        "ajax": 'article/data-table',
        'language' : {
            'url': '//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json'
        },
        "columns": [
            { "data": "name" },
            { "data": "alias" },
            { "data":
                "active",
                "render": function (data, type, row) {
                    var template = jQuery('#rowActiveColumn').html();
                    Mustache.parse(template);
                    return Mustache.render(template, row);
                }
            },
            { "data":
                "created_at" ,
                "render": function (data, type, row) {
                    return row.created_at ? moment(row.created_at).format(dateFormat) : 'Дата не установлена';
                }
            },
            { "data":
                "updated_at",
                "render": function (data, type, row) {
                    return row.updated_at ? moment(row.updated_at).format(dateFormat) : 'Дата не установлена';
                }
            },
            { "data":
                "action",
                'render':  function (data, type, row) {
                    var template = jQuery('#actionButtonsTemplate').html();
                    Mustache.parse(template);
                    return Mustache.render(template, row);
                }
            }
        ]
    });

    // Удаление статьи
    jQuery('body').on('click', '.btn-post-destroy', function () {
        var element = jQuery(this),
            id = element.data('id'),
            name = element.data('article-name'),
            template = jQuery('#destroy-article').html();

        Mustache.parse(template);

        jQuery.confirm({
            title: 'Уделаение статьи',
            content: Mustache.render(template, {'name': name}),
            type: 'red',
            buttons: {
                'Удалить': {
                    btnClass: 'btn-danger',
                    action: function () {
                        jQuery.ajax({
                            url: '/admin/article/' + id,
                            type: 'DELETE',
                            headers: { 'X-CSRF-TOKEN': _token },
                            data: {
                                id: id
                            }
                        }).done(function (response) {
                            if (response.isError === true) {
                                errorMessage(response.message);
                                return false;
                            }

                            // Обновление таблицы
                            table.DataTable().ajax.reload();

                            sucessMessage(response.message);
                        });
                    }
                },
                'Отмена': {}
            }
        });
    });

    //  Переключатель статуса статьи
    jQuery('body').on('click', '.btn-post-active', function () {
        var element = jQuery(this),
            id = element.data('id'),
            active = element.data('active');

        if (active > 0) {
            active = 0;
        } else {
            active = 1;
        }

        jQuery.ajax({
            url: '/admin/article/active',
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': _token },
            data: {
                id: id,
                active: active
            }
        }).done(function (response) {
            if (response.isError === true) {
                errorMessage(response.message);
                return false;
            }

            // Обновление таблицы
            table.DataTable().ajax.reload();

            sucessMessage(response.message);
        });
    });
});