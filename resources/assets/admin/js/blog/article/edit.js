jQuery(function () {
    var form = jQuery('#edit-article-form'),
        contentEditor = jQuery('#content'),
        alert = jQuery('#alert');

    // Валидация формы
    // form.validate({
    //     ignore: '.ignore',
    //     rules: {
    //         name: {
    //             required: true,
    //             minlength: 3
    //         },
    //         alias: {
    //             required: true,
    //             minlength: 3
    //         },
    //         description: {
    //             required: true
    //         },
    //         content: {
    //             required: function() {
    //                 return contentEditor.text().length === 0;
    //             }
    //         },
    //         media: {
    //             required: function () {
    //                 return jQuery('#files .item').length === 0;
    //             }
    //         }
    //     }
    // });

    // Отправка данных формы
    form.ajaxForm({
        method: 'POST',
        dataType: 'JSON',
        // beforeSubmit: function () {
        //     return form.valid();
        // },
        data: {
            content: function () {
                contentEditor.wysiwyg();
                return contentEditor.text()
            },
            data: 'dffdfdf',
            _method: 'PUT',
            _token: _token
        },
        success: function (response) {
            if (response.isError === true) {
                errorMessage(response.message);
                return false;
            }

            sucessMessage(response.message);
        },
        error: function (response) {
            alert.find('.messages').empty();
            jQuery.each(response.responseJSON, function (index, val) {
                alert
                    .find('.messages')
                    .append(jQuery('<p/>').text(val));
            });
            alert.removeClass('hide');
            return false;
        }
    });
});