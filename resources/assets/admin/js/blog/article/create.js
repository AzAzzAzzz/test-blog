jQuery(function () {
    var form = jQuery('#create-article-form'),
        contentEditor = jQuery('#content'),
        alert = jQuery('#alert');

    // Валидация формы
    form.validate({
        ignore: [],
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            alias: {
                required: true,
                minlength: 3
            },
            description: {
                required: true
            },
            content: {
                required: function() {
                    return contentEditor.text().length === 0;
                }
            },
            media: {
                required: true
            }
        }
    });

    // Отправка данных формы
    form.ajaxForm({
        method: 'POST',
        data: {
            content: function () {
                contentEditor.wysiwyg();
                console.log(contentEditor.text());
                return contentEditor.text()
            }
        },
        success: function (response) {
            if (response.isError === true) {
                errorMessage(response.message);
                return false;
            }

            // Очистка формы
            contentEditor.text('');
            jQuery('.item').remove();
            jQuery(form).trigger('reset');

            sucessMessage(response.message);
        },
        error: function (response) {
            alert.find('.messages').empty();
            jQuery.each(response.responseJSON, function (index, val) {
                alert
                    .find('.messages')
                    .append(jQuery('<p/>').text(val));
            });
            alert.removeClass('hide');
            return false;
        }
    });
});