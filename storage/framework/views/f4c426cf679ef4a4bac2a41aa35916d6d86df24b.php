<?php $__env->startSection('title', 'Главная'); ?>

<?php $__env->startSection('page'); ?>

    <?php if( ! empty($articles)): ?>
        <div class="container">
            <h1 class="my-4">
                Список статей
            </h1>

            <div class="content">
                <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="row">
                        <div class="col-md-7">
                            <a href="<?php echo e(route('article.post',  $article->alias)); ?>" target="_blank">
                                <img class="img-fluid rounded mb-3 mb-md-0 responsive" src="<?php echo e($article->media[0]->getUrl()); ?>" alt="">
                            </a>
                        </div>
                        <div class="col-md-5">
                            <h3><?php echo e($article->name); ?></h3>
                            <p class="datetime"><?php echo e($article->created_at); ?></p>
                            <p>
                                <?php echo e($article->description); ?>

                            </p>
                            <a class="btn btn-primary" href="<?php echo e(route('article.post',  $article->alias)); ?>" target="_blank">Далее</a>
                        </div>
                    </div>

                    <hr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <div class="row text-center">
                    <?php echo e($articles->links()); ?>

                </div>
            </div>
    <?php else: ?>
        <p class="alert-danger text-center">Не удалось загрузить данные</p>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>
    ##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
    <?php echo e(Html::style(mix('assets/app/css/blog/blog.css'))); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
    <?php echo e(Html::script(mix('assets/app/js/blog/index.js'))); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>