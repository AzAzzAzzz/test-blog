<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Jenssegers\Date\Date;

class Article extends Model
{
    use HasMediaTrait;

    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->width(370)
            ->height(275)
            ->nonQueued();
    }

    public function getCreatedAtAttribute($value)
    {
        Date::setLocale('ru');
        return Date::parse($value)->format('j F Y H:i');
    }
}
