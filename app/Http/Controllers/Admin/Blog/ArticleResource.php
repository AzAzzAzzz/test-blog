<?php

namespace App\Http\Controllers\Admin\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog\Article;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests\StoreArticle;
use App\Http\Requests\UpdateArticle;
use Spatie\MediaLibrary\Media;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class ArticleResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.blog.article.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreArticle $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreArticle $request)
    {
        $article = new Article();

        $article->name        = $request->get('name');
        $article->alias       = $request->get('alias');
        $article->content     = $request->get('content');
        $article->description = $request->get('description');
        $article->active      = (int) $request->get('active') ;

        // Добавление данных статьи
        $result = $article->save();

        // Получение прикреленного файла
        $media = $request->file('media');

        // Привязка файла
        $article->addMedia($media)
            ->withCustomProperties(['article_id' => $article->id])
            ->toMediaLibrary('images');

        if ( ! $result) {
            $this->response['isError'] = true;
            $this->response['message'] = Lang::get('blog.article.store_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('blog.article.store_success');
        return response()->json($this->response);
    }

    /**
     * Переключатель статуса активности статьи
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function active (Request $request)
    {
        $article = Article::find($request->id);

        if (empty($article)) {
            $this->response['isError'] = true;
            $this->response['responseMsg'] = Lang::get('blog.article.active_error');
            return response()->json($this->response);
        }

        $article->active = $request->get('active');

        // Сохранение изменений в модели
        $result = $article->save();

        if ( ! $result) {
            $this->response['isError']     = true;
            $this->response['message'] = Lang::get('blog.article.active_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('blog.article.active_success');
        return response()->json($this->response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.blog.article.edit', ['article' => Article::with('media')->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateArticle $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateArticle $request, $id)
    {
        // Начало транзакции
        DB::beginTransaction();

        $article =  Article::with('media')->find($id);

        $article->name        = $request->get('name');
        $article->alias       = $request->get('alias');
        $article->description = $request->get('description');
        $article->content     = $request->get('content');
        $article->active      = (int) $request->get('active');

        // Обновление данных статьи
        $result = $article->save();

        // Получение прикреленного файла
        $media = $request->file('media');

        if ($media && $result) {
            // Удаление модели файла
            $result = Media::find($article->media[0]->id)->delete();

            if ( ! $result) {
                DB::rollback();
                $this->response['isError'] = true;
                $this->response['message'] = Lang::get('blog.article.update_error');
                return response()->json($this->response);
            }

            // Удаление прикрепленнго файла с диска
            File::deleteDirectory(config("filesystems.disks.media.root") . '/' .  $article->media[0]->id);

            // Привязка нвого файла
            $article->addMedia($media)
                ->withCustomProperties(['article_id' => $article->id])
                ->toMediaLibrary('images');
        }

        // Конец транзакции
        DB::commit();

        if ( ! $result) {
            $this->response['isError'] = true;
            $this->response['message'] = Lang::get('blog.article.update_error');
            return response()->json($this->response);
        }

        $this->response['message'] = Lang::get('blog.article.update_success');
        return response()->json($this->response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->response['isError'] = true;
        $this->response['message'] = Lang::get('blog.article.destroy_error');

        // Начало транзакции
        DB::beginTransaction();

        // Удаление модели статьи
        $result = Article::where('id', $id)->delete();

        if ( ! $result) {
            DB::rollback();
            return response()->json($this->response);
        }

        // Модель файла
        $media = Media::where('model_id', $id)->first();

        // Удаление модели  файла
        $result = $media->delete();

        if ( ! $result) {
            DB::rollback();
            return response()->json($this->response);
        }

        // Удаление прикрепленнго файла с диска
        File::deleteDirectory(config("filesystems.disks.media.root") . '/' .  $media->id);

        // Конец транзакции
        DB::commit();

        $this->response['isError'] = false;
        $this->response['message'] = Lang::get('blog.article.destroy_success');
        return response()->json($this->response);
    }

    /**
     * Врзвращает список статей для DataTable
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData ()
    {
        return response()->json([
            'data' => Article::all()
        ]);
    }
}
