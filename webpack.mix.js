const {mix} = require('laravel-mix');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// paths to clean
var pathsToClean = [
    'public/assets/app/js',
    'public/assets/app/css',
    'public/assets/admin/js',
    'public/assets/admin/css',
    'public/assets/auth/css',
];

// the clean options to use
var cleanOptions = {};

mix.webpackConfig({
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions)
    ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Core
 |--------------------------------------------------------------------------
 |
 */

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/pace-progress/pace.js',

], 'public/assets/app/js/app.js').version();

mix.styles([
    'node_modules/font-awesome/css/font-awesome.css',
    'node_modules/pace-progress/themes/blue/pace-theme-minimal.css',
    'node_modules/bootstrap/dist/css/bootstrap.css'
], 'public/assets/app/css/app.css').version();

mix.copy([
    'node_modules/font-awesome/fonts/',
], 'public/assets/app/fonts');

/*
 |--------------------------------------------------------------------------
 | Auth
 |--------------------------------------------------------------------------
 |
 */

mix.styles('resources/assets/auth/css/login.css', 'public/assets/auth/css/login.css').version();
mix.styles('resources/assets/auth/css/register.css', 'public/assets/auth/css/register.css').version();
mix.styles('resources/assets/auth/css/passwords.css', 'public/assets/auth/css/passwords.css').version();

mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/gentelella/vendors/animate.css/animate.css',
    'node_modules/gentelella/build/css/custom.css',
], 'public/assets/auth/css/auth.css').version();

/*
 |--------------------------------------------------------------------------
 | Admin
 |--------------------------------------------------------------------------
 |
 */

mix.scripts([
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
    'node_modules/bootstrap-wysiwyg/src/bootstrap-wysiwyg.js',
    'node_modules/bootstrap-switch/dist/js/bootstrap-switch.js',
    'node_modules/gentelella/build/js/custom.js',
    'node_modules/datatables.net/js/jquery.dataTables.js',
    'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    'node_modules/datatables.net-buttons/js/dataTables.buttons.js',
    'node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js',
    'node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.js',
    'node_modules/datatables.net-keytable/js/dataTables.keyTable.js',
    'node_modules/datatables.net-responsive/js/dataTables.responsive.js',
    'node_modules/datatables.net-responsive-bs/js/responsive.bootstrap.js',
    'node_modules/datatables.net-scroller/js/dataTables.scroller.js',
    'node_modules/moment/moment.js',
    'node_modules/moment/locale/ru.js',
    'node_modules/mustache/mustache.js',
    'node_modules/jquery-confirm/dist/jquery-confirm.min.js',
    'node_modules/jquery-validation/dist/jquery.validate.js',
    'node_modules/jquery-validation/dist/buttons.flash.js',
    'node_modules/jquery-validation/dist/buttons.html5.js',
    'node_modules/jquery-validation/dist/buttons.print.js',
    'node_modules/jquery-validation/dist/localization/messages_ru.js',
    'node_modules/jquery-form/src/jquery.form.js',
    'node_modules/dropzone/dist/dropzone.js',
    'resources/assets/admin/js/app.js'

], 'public/assets/admin/js/admin.js').version();

mix.sass('resources/assets/admin/sass/app.scss', 'public/assets/admin/css/admin.css');

mix.styles([
    'node_modules/gentelella/build/css/custom.css'
], 'public/assets/admin/css/custom.css').version();


mix.copy([
    'node_modules/gentelella/vendors/bootstrap/dist/fonts',
], 'public/assets/admin/fonts');


mix.scripts([
    'node_modules/select2/dist/js/select2.full.js',
    'resources/assets/admin/js/users/edit.js',
], 'public/assets/admin/js/users/edit.js').version();

mix.styles([
    'node_modules/select2/dist/css/select2.css',
], 'public/assets/admin/css/users/edit.css').version();

mix.scripts([
    'node_modules/gentelella/vendors/Flot/jquery.flot.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.time.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.pie.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.stack.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.resize.js',

    'node_modules/gentelella/production/js/flot/jquery.flot.orderBars.js',
    'node_modules/gentelella/production/js/flot/date.js',
    'node_modules/gentelella/production/js/flot/curvedLines.js',
    'node_modules/gentelella/production/js/flot/jquery.flot.spline.js',

    'node_modules/gentelella/production/js/moment/moment.min.js',
    'node_modules/gentelella/production/js/datepicker/daterangepicker.js',


    'node_modules/gentelella/vendors/Chart.js/dist/Chart.js',

    'resources/assets/admin/js/dashboard.js',
], 'public/assets/admin/js/dashboard.js').version();

mix.styles([
    'resources/assets/admin/css/dashboard.css',
], 'public/assets/admin/css/dashboard.css').version();

// BLog
mix.scripts('resources/assets/admin/js/blog/article/index.js', 'public/assets/admin/js/blog/article/index.js');
mix.scripts('resources/assets/admin/js/blog/article/edit.js', 'public/assets/admin/js/blog/article/edit.js');
mix.scripts('resources/assets/admin/js/blog/article/create.js', 'public/assets/admin/js/blog/article/create.js');


/*
 |--------------------------------------------------------------------------
 | Frontend
 |--------------------------------------------------------------------------
 |
 */

mix.sass('resources/assets/sass/blog/blog.scss', 'public/assets/app/css/blog/blog.css');

mix.scripts('resources/assets/app/js/blog/index.js', 'public/assets/app/js/blog/index.js');